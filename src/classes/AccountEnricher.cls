/**
 * @author pedrogleta - topi
 */
public with sharing class AccountEnricher {

    public List<Event> enrichEvents (List<Account> accounts) {

        List<Event> events = new List<Event>();

        for (Account account : accounts) {
            
            Datetime eventStartDateTime = account.CreatedDate.addDays(1);
            Datetime eventEndDateTime = eventStartDateTime.addHours(1);

            Event event = new Event(Description = 'Apresentar o portifolio de Produtos para o novo cliente com enfoque em nossa carteira Premium', Subject = 'Apresentação instrucional dos produtos Premium', StartDateTime = eventStartDateTime, EndDateTime = eventEndDateTime, WhatId = account.Id, ContactType__c = 'Telefônico');

            events.add(event);

        }

        return events;

    }

}

