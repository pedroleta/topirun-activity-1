/**
 * @author pedrogleta - topi
 */
@isTest
public class AccountEnricherTest {

    @isTest
    static void enrichEvents () {

        // Map<String, SObject> records = AccountFixtureFactory.createIntegratedScenario();

        List<Account> accounts = new List<Account> {
            // (Account) records.get('premium-account')
            // AccountFixtureFactory.createAccount('Test Account', 'Premium', 1000000)
            new Account(Name = 'Enricher Test Account', Category__c = 'Premium', AnnualRevenue = 1000000)
        };

        insert accounts;

        AccountEnricher enricher = new AccountEnricher();

        List<Event> events = enricher.enrichEvents(accounts);

        System.assert(!events.isEmpty());

    }

}