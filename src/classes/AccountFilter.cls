/**
 * @author pedrogleta - topi
 */
public with sharing class AccountFilter {
    public List<Account> filterByAnnualRevenueOverAMillionAndIsPremium (List<Account> newAccounts, Map<Id,Account> oldAccounts) {
        List<Account> filteredAccounts = new List<Account>();

        for (Account account : newAccounts) {
            if (account.AnnualRevenue >= 1000000 && account.Category__c == 'Premium') {
                filteredAccounts.add(account);
            }
        }

        return filteredAccounts;
    }

}