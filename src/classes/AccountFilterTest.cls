/**
 * @author pedrogleta - topi
 */
@isTest
public class AccountFilterTest {

    @isTest
    static void filterByAnnualRevenueOverAMillionAndIsPremium () {

        Map<String,SObject> records = AccountFixtureFactory.createIntegratedScenario();

        List<Account> accounts = new List<Account> {
            (Account) records.get('premium-account'),
            (Account) records.get('standard-account')
        };

        AccountFilter filter = new AccountFilter();

        List<Account> filteredAccounts = filter.filterByAnnualRevenueOverAMillionAndIsPremium(accounts, null);

        System.assert(filteredAccounts.contains((Account) records.get('premium-account')));
        System.assert(!filteredAccounts.contains((Account) records.get('standard-account')));

    }

}