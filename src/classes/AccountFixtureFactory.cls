/**
 * @author pedrogleta - topi
 */
@isTest
public class AccountFixtureFactory {

    public static Account newAccount (String name, String category, Integer annualRevenue) {

        return new Account (Name = name, Category__c = category, AnnualRevenue = annualRevenue);

    }

    public static Account createAccount (String name, String category, Integer annualRevenue) {

        return createAccount(newAccount(name, category, annualRevenue));

    }

    public static Account createAccount (Account account) {

        insert account;
        return account;

    }

    public static Map<String, SObject> createIntegratedScenario () {

        Map<String,SObject> records = new Map<String,SObject>();

        Account premiumAccount = createAccount('Premium Account', 'Premium', 1000000);
        records.put('premium-account', premiumAccount);
        Account standardAccount = createAccount('Standard Account', 'Standard', 12345);
        records.put('standard-account', standardAccount);

        return records;

    }

}