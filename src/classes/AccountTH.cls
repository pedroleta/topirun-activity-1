/**
    @author pedrogleta - topi
 */
public virtual with sharing class AccountTH extends TriggerHandler {

    List<Account> newAccounts;

    Map<Id,Account> oldAccounts;

    AccountFilter filter;

    AccountEnricher enricher;

    public AccountTh () {
        this ((List<Account>) Trigger.new, (Map<Id,Account>) Trigger.oldMap);
    }

    public AccountTH (List<Account> newAccounts, Map<Id,Account> oldAccounts) {
        this.newAccounts = newAccounts;
        this.oldAccounts = oldAccounts;
    
        this.filter = new AccountFilter();
        this.enricher = new AccountEnricher();
        // this.validator = new AccountValidator();
    }

    override
    public void afterInsert() {
        updateEvents();
    }

    virtual
    public void updateEvents () {
        List<Account> filteredAccounts = this.filter.filterByAnnualRevenueOverAMillionAndIsPremium(newAccounts, oldAccounts);

        List<Event> events = this.enricher.enrichEvents(filteredAccounts);

        insert events;
    }

}